<?php

namespace App\Controller;

use App\Entity\Task;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ToDoListController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        $tasks = $this->getDoctrine()->getRepository(Task::class)->findBy([], ['id' => 'DESC']);

        return $this->render('index.html.twig', ['tasks'=> $tasks]);
    }



    /**
     * @Route("/create", name="create", methods={"POST"})
     */
    public function create(Request $request)
    {
        $title = trim($request->request->get('title'));

        if(empty($title)){
            return $this->redirectToRoute('home');
        }

        $entityManager = $this->getDoctrine()->getManager();

        $task = new Task;

        $task->setTitle($title);


        $entityManager->persist($task);

        $entityManager->flush();

        return $this->redirectToRoute('home');

    }




    /**
     * @Route("/check/{id}", name="check")
     */
    public function switchStatus(Task $id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $task = $entityManager->getRepository(Task::class)->find($id);
    
        $task->setStatus(! $task->getStatus());

        $entityManager->flush();

        return $this->redirectToRoute('home');
    }



    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $task = $entityManager->getRepository(Task::class)->find($id);

        $entityManager->remove($task);

        $entityManager->flush();

        return $this->redirectToRoute('home');
    }
}
